package matrix;

import heterostructures.Semiconductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SemiconductorMatrix implements Matrix {
    Semiconductor semiconductor;

    private static final int SIZE = 2;

    public SemiconductorMatrix() {
    }

    public SemiconductorMatrix(Semiconductor semiconductor) {
        this.semiconductor = semiconductor;
    }

    private double ch(double x) {
        return (Math.exp(x)+Math.exp(-x))/2;
    }

    private double sh(double x) {
        return (Math.exp(x)-Math.exp(-x))/2;
    }

    private double k(){
        return 0;
    }

    private double fi(double d, double k){
        return (2*Math.PI*k*d)/ semiconductor.getLamX();
    }

    public double[][][] matrix() {
        double[][][] m = new double[SIZE][SIZE][SIZE];
        m[0][0][0] = Math.cos(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k()));
        m[0][1][0] = (semiconductor.n() * Math.sin(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k())) + k() * Math.cos(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()))) / (semiconductor.n() * semiconductor.n() + k() * k());
        m[1][0][0] = semiconductor.n() * Math.sin(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k())) - k() * Math.cos(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()));
        m[1][1][0] = Math.cos(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k()));
        m[0][0][1] = Math.sin(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()));
        m[0][1][1] = (k() * Math.sin(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k())) - semiconductor.n() * Math.cos(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()))) / (semiconductor.n() * semiconductor.n() + k() * k());
        m[1][0][1] = -k() * Math.sin(fi(semiconductor.d(), semiconductor.n())) * ch(fi(semiconductor.d(), k())) - semiconductor.n() * Math.cos(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()));
        m[1][1][1] = Math.sin(fi(semiconductor.d(), semiconductor.n())) * sh(fi(semiconductor.d(), k()));
        return m;
    }

    public SemiconductorMatrix setSemiconductor(Semiconductor semiconductor) {
        this.semiconductor = semiconductor;
        return this;
    }

    public Semiconductor getSemiconductor() {
        return semiconductor;
    }
}
