package matrix;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class MatrixConfiguration {
    @Bean
    E e(){
        return new E();
    }
}
