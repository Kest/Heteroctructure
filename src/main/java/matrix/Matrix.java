package matrix;

public interface Matrix {
    double[][][] matrix();
}
