package heterostructures;

import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import parameters.semiconductor.SParameters;

@Component
public interface Semiconductor {

    double n();

    int getLamX();

    double d();

    Semiconductor setsParameters(SParameters sParameters);
}
