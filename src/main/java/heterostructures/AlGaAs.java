package heterostructures;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import parameters.semiconductor.SParameters;

@Component
public class AlGaAs implements Semiconductor {

    SParameters sParameters;

    public double n() {
        double E0 = 3.65 + 0.871 * sParameters.getX() + 0.179 * sParameters.getX() * sParameters.getX();
        double Ed = 36.1 - 2.45 * sParameters.getX();
        double E = 1239.85 / sParameters.getLamX();
        return Math.sqrt((E0 * Ed) / (E0 * E0 - E * E) + 1);
    }

    @Override
    public int getLamX() {
        return sParameters.getLamX();
    }

    @Override
    public double d() {
        return sParameters.getD();
    }

    public AlGaAs setsParameters(SParameters sParameters) {
        this.sParameters = sParameters;
        return this;
    }
}
