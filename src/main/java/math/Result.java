package math;

import org.springframework.stereotype.Component;

@Component
public class Result {
    double[][][] result = new double[2][2][2];

    public double[][][] get() {
        return result;
    }

    public void set(double[][][] result) {
        this.result = result;
    }
}
