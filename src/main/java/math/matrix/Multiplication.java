package math.matrix;

public class Multiplication {

    public static double[][][] AxB(double[][][] A, double[][][] B){
        double[][][] matrixResult = new double[2][2][2];
        matrixResult[0][0][0] = A[0][0][0] * B[0][0][0] - A[0][0][1] * B[0][0][1] - A[0][1][0] * B[1][0][0] + A[0][1][1] * B[1][0][1];
        matrixResult[0][1][0] = A[0][0][0] * B[0][1][0] - A[0][0][1] * B[0][1][1] + A[0][1][0] * B[1][1][0] - A[0][1][1] * B[1][1][1];
        matrixResult[1][0][0] = A[1][1][0] * B[1][0][0] - A[1][1][1] * B[1][0][1] + A[1][0][0] * B[0][0][0] - A[1][0][1] * B[0][0][1];
        matrixResult[1][1][0] = A[1][1][0] * B[1][1][0] - A[1][1][1] * B[1][1][1] - A[1][0][0] * B[0][1][0] + A[1][0][1] * B[0][1][1];
        matrixResult[0][0][1] = A[0][0][0] * B[0][0][1] + A[0][0][1] * B[0][0][0] - A[0][1][0] * B[1][0][1] - A[0][1][1] * B[1][0][0];
        matrixResult[0][1][1] = A[0][0][0] * B[0][1][1] + A[0][0][1] * B[0][1][0] + A[0][1][0] * B[1][1][1] + A[0][1][1] * B[1][1][0];
        matrixResult[1][0][1] = A[1][1][0] * B[1][0][1] + A[1][1][1] * B[1][0][0] + A[1][0][0] * B[0][0][1] + A[1][0][1] * B[0][0][0];
        matrixResult[1][1][1] = A[1][1][0] * B[1][1][1] + A[1][1][1] * B[1][1][0] - A[1][0][0] * B[0][1][1] - A[1][0][1] * B[0][1][0];
        return matrixResult;
    }
}
