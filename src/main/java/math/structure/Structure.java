package math.structure;

import configuration.AllConfiguration;
import heterostructures.AlGaAs;
import math.Result;
import math.matrix.Multiplication;
import matrix.E;
import matrix.SemiconductorMatrix;
import optics.matrix.Reflection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import parameters.semiconductor.SParameters;
import parameters.structure.StructureParameters;

@Component
public class Structure {

    @Autowired
    AlGaAs alGaAs;

    @Autowired
    StructureParameters structureParameters;

    @Autowired
    Result result;

    @Autowired
    SemiconductorMatrix semiconductorMatrix;

    @Autowired
    Reflection reflection;

    @Autowired
    E e;

    public void calc(){
        alGaAs.setsParameters(structureParameters.getList().get(0));
        result.set(Multiplication.AxB(e.matrix(), semiconductorMatrix.matrix()));
        for (int i = 1; i < structureParameters.getList().size(); i++) {
            alGaAs.setsParameters(structureParameters.getList().get(i));
            Multiplication.AxB(result.get(), semiconductorMatrix.matrix());
        }
    }

    public double R(){
        return reflection.get();
    }

}
