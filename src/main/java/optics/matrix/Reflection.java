package optics.matrix;

import math.Result;
import org.springframework.stereotype.Component;

@Component
public class Reflection {

    private double n0, nl;
    private double kl;

    Result result;

    public Reflection(Result result) {
        this.result = result;
        n0 = 1;
        nl = 1;
        kl = 0;
    }

    public Reflection(double n0, double nl, double kl){
        this.n0 = n0;
        this.nl = nl;
        this.kl = kl;
    }



    public double get(){

        double V = n0 * result.get()[0][0][0] + result.get()[1][0][1] - nl * (n0 * result.get()[0][1][1] + result.get()[1][1][0]) + kl * (n0 * result.get()[0][1][0] - result.get()[1][1][1]);
        double Z = n0 * result.get()[0][0][1] - result.get()[1][0][0] + nl * (n0 * result.get()[0][1][0] - result.get()[1][1][1]) + kl * (n0 * result.get()[0][1][1] + result.get()[1][1][0]);
        double X = n0 * result.get()[0][0][0] - result.get()[1][0][1] - nl * (n0 * result.get()[0][1][1] - result.get()[1][1][0]) + kl * (n0 * result.get()[0][1][0] + result.get()[1][1][1]);
        double Y = n0 * result.get()[0][0][1] + result.get()[1][0][0] + nl * (n0 * result.get()[0][1][0] + result.get()[1][1][1]) + kl * (n0 * result.get()[0][1][1] - result.get()[1][1][0]);

        return (V * V + Z * Z) / (X * X + Y * Y);
    }

}
