package aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect {

    @Pointcut("execution(double heterostructures.*.n())")
    public void before() {

    }

    @Before("before()")
    public void beforeN(){
        System.out.println("beforeN");
    }

    @Pointcut("execution(double optics.matrix.Reflection.get())")
    public void getResult(){

    }

    @Around("getResult()")
    public Object get(ProceedingJoinPoint pjp) {
        return 6;
    }
}
