package aspects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AspectsConfiguration {
    @Bean
    LogAspect logAspect(){
        return new LogAspect();
    }
}
