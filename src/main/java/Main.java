import configuration.AllConfiguration;
import math.structure.Structure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AllConfiguration.class);

        Structure structure = context.getBean(Structure.class);
        structure.calc();
        System.out.println(structure.R());
    }
}
