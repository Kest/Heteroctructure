package configuration;

import aspects.AspectsConfiguration;
import aspects.LogAspect;
import heterostructures.AlGaAs;
import heterostructures.Semiconductor;
import heterostructures.SemiconductorConfiguration;
import math.Result;
import math.structure.Structure;
import matrix.MatrixConfiguration;
import matrix.SemiconductorMatrix;
import optics.matrix.Reflection;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import parameters.ParametersConfiguration;

@Configuration
@Import({SemiconductorConfiguration.class, MatrixConfiguration.class, ParametersConfiguration.class, AspectsConfiguration.class})
public class AllConfiguration {

    @Bean
    Structure structure(){
        return new Structure();
    }

    @Bean
    @Scope("singleton")
    AlGaAs alGaAs(){
        return new AlGaAs();
    }

    @Bean
    SemiconductorMatrix semiconductorMatrix(){return new SemiconductorMatrix(alGaAs());}

    @Bean
    Result result(){
        return new Result();
    }

    @Bean
    Reflection reflection(Result result){
        return new Reflection(result);
    }
}
