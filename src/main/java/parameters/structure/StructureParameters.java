package parameters.structure;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import parameters.semiconductor.SParameters;

import java.io.*;
import java.util.List;

@Component
public class StructureParameters {

    List<SParameters> list;

    public StructureParameters() {
    }

    public StructureParameters create(){
        try {
            Gson gson = new Gson();
            FileReader fileReader = new FileReader("/Users/macbook/IdeaProjects/my/Heterostructure/src/main/resources/semiconductor.json");
            StructureParameters structureParameters = gson.fromJson(fileReader, StructureParameters.class);
            list = structureParameters.getList();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return this;
    }

    public List<SParameters> getList() {
        return list;
    }

    public void setList(List<SParameters> list) {
        this.list = list;
    }

    public static void main(String[] args) throws IOException {
        StructureParameters structureParameters = new StructureParameters().create();
        System.out.println(structureParameters.getList());
    }
}
