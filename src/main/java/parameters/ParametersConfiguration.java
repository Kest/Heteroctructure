package parameters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import parameters.semiconductor.SParameters;
import parameters.structure.StructureParameters;

@Configuration
public class ParametersConfiguration {
    @Bean
    SParameters sParameters(){
        return new SParameters();
    }

    @Bean
    StructureParameters structureParameters(){
        return new StructureParameters().create();
    }
}
