package parameters.semiconductor;

public class SParameters {
    private int lamX;
    private double x;
    private double d;

    public SParameters(int lamX, double x, double d) {
        this.lamX = lamX;
        this.x = x;
        this.d = d;
    }

    public SParameters() {
    }

    public int getLamX() {
        return lamX;
    }

    public void setLamX(int lamX) {
        this.lamX = lamX;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    @Override
    public String toString() {
        return lamX + " " + d + " " + x;
    }
}
